#!/bin/bash

# Init evnironement variable
export DEBIAN_FRONTEND=noninteractive

# Install basic dependencies
sudo apt-get -y update
sudo apt-get -y install git curl

# Install Node version 16
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g yarn

# Install Solana CLI
sh -c "$(curl -sSfL https://release.solana.com/v1.9.5/install)"

# Install and start metaplex
git clone https://github.com/metaplex-foundation/metaplex.git
cd metaplex/js
yarn install && yarn bootstrap && yarn build
yarn start
