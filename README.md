1. Run script 

`wget -O - https://bitbucket.org/madisearth/metaplex-boot/raw/ed8247d50a0b9b73ece80917688325ae01bed374/metaplex-boot.sh | bash`

2. Add Solana CLI to PATH

`export PATH="$HOME/.local/share/solana/install/active_release/bin:$PATH"`

